def stop_words(*words):
    def decorator_maker(func):
        def wrapper(*args, **kwargs):
            st = func(*args, **kwargs)
            for i in words[0]:
                st = st.replace(i, '*')   
            return st
        return wrapper
    return decorator_maker

@stop_words(['pepsi', 'BMW'])
def create_slogan(name: str) -> str:
    return f"{name} drinks pepsi in his brand new BMW!"

assert create_slogan("Steve") == "Steve drinks * in his brand new *!"