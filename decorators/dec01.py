import functools

def logger(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        print(func.__name__)
    return wrapper
    

@logger
def add(x, y):
    return x + y

@logger
def square_all(*args):
    return [arg ** 2 for arg in args]

add(1, 2)
square_all(1, 2)
