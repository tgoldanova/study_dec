import functools

def arg_rules(type_: str, max_length: int, contains: list):
    def decorator_maker(func):
        def wrapper(name):

            if type(name) == str:
                if len(name) <= max_length:
                    for i in contains:
                        if i in name:
                            return func(name)
                        else: 
                            print('Contains not found')
                else:
                    print('LengthError')
            else:
                print('Type')
            return False 
        return wrapper
    return decorator_maker


@arg_rules(type_=str, max_length=15, contains=['05', '@'])
def create_slogan(name: str):
    return f"{name} drinks pepsi in his brand new BMW!"

assert create_slogan('johndoe05@gmail.com') is False
assert create_slogan('S@SH05') == 'S@SH05 drinks pepsi in his brand new BMW!'